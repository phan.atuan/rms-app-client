# Architecture Diagram

![alt architecture diagram](architecture-diagram.png)

### Requirements

- [Install the AWSMobile CLI](https://github.com/aws/awsmobile-cli)
- And follow instruction to enable user-signin, cloud-api
- Update configuration information in src/aws-exports.js

### Usage

To fetch dependencies

``` bash
$ npm install
```

To run unit test

``` bash
$ npm run test
```

To start the application

``` bash
$ npm run start
```

