
const Amplify = jest.genMockFromModule('aws-amplify');

Amplify.Auth.currentSession = jest.fn().mockImplementation(() => {
  // Object.defineProperty(module.exports,'Auth_currentSession_promise',
  //   { value: Promise.resolve (true)})
  module.exports.Auth_currentSession_promise = new Promise(resolve =>
    process.nextTick(() => resolve(true)));
  // module.exports.Auth_currentSession_promise = Promise.reject (new Error('No current user'));
  return module.exports.Auth_currentSession_promise;
});

Amplify.Auth.signIn = jest.fn().mockImplementation((email, password) => {
  module.exports.Auth_signIn_promise = new Promise(resolve =>
    process.nextTick(() => resolve({ email, password })));
  return module.exports.Auth_signIn_promise;
});

Amplify.Auth.signUp = jest.fn().mockImplementation(() => {
  module.exports.Auth_signUp_promise = new Promise(resolve =>
    process.nextTick(() => resolve({})));
  return module.exports.Auth_signUp_promise;
});

Amplify.Auth.confirmSignUp = jest.fn().mockImplementation(() => {
  module.exports.Auth_confirmSignUp_promise = new Promise(resolve =>
    process.nextTick(() => resolve(true)));
  return module.exports.Auth_confirmSignUp_promise;
});

module.exports = Amplify;
