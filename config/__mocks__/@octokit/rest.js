let octokit;

module.exports = () => {
  if (octokit) return octokit;
  octokit = Object.create(null);
  octokit.repos = {
    getForUser: () => {
      // return Promise.resolve({data: [{name: 'repo1'}]})

      octokit.repos.getForUser._promise = new Promise(resolve =>
        process.nextTick(() => resolve({ data: [{ name: 'repo1' }] })));
      return octokit.repos.getForUser._promise;
    },
  };
  return octokit;
};
