import { configure, setAddon } from '@storybook/react';
import JSXAddon from 'storybook-addon-jsx';

function loadStories() {
  require('../src/stories');
  require('../src/stories/example');
}

setAddon(JSXAddon);
configure(loadStories, module);
