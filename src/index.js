import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import ReactDOM from 'react-dom';
import Amplify from 'aws-amplify';
import { Provider } from 'react-redux';

import './index.css';
import App from './containers/app/App';
// import registerServiceWorker from './registerServiceWorker';
import awsExports from './aws-exports';
// import apiConfig from './api.config';
import storeConfig from './store-config';

const store = storeConfig();
// const config = Object.assign({}, awsExports, apiConfig);
Amplify.configure(awsExports);
/*
Amplify.configure({
  Auth: {
    mandatorySignIn: true,
    region: config.aws_cognito_region,
    userPoolId: config.aws_user_pools_id,
    identityPoolId: config.aws_cognito_identity_pool_id,
    userPoolWebClientId: config.aws_user_pools_web_client_id,
  },
  API: {
    endpoints: [
      {
        name: 'request',
        endpoint: config.api.request.endpoint,
        region: config.api.request.region,
      },
    ],
  },
});
*/
ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById('root'),
);
// registerServiceWorker();

