/*
import {
  FETCH_REQUEST_IN_PROGRESS, FETCH_REQUEST_COMPLETED,
  SAVE_REQUEST_IN_PROGRESS, SAVE_REQUEST_COMPLETED,
  REQUEST_PROP_CHANGED, RESET_FORM_DATA, UNEXPECTED_ERROR_HAPPENED,
  FETCH_REQUEST_LIST_IN_PROGRESS, FETCH_REQUEST_LIST_COMPLETED,
  DELETE_REQUEST_IN_PROGRESS, DELETE_REQUEST_COMPLETED, CLOSE_REQUEST_SUCCESSFULLY,
} from '../../actions/request';
*/
/*
export default function request(state = {
  list: {
    isFetching: false,
    isSubmitting: false,
    items: [],
    errors: null,
    page: 1,
  },
  form: {
    isFetching: false,
    isSubmitting: false,
    data: {},
    errors: null,
  },
}, action) {
  const { type } = action;
  const {
    list,
    form,
  } = state;

  switch (type) {
    case FETCH_REQUEST_COMPLETED:
    case SAVE_REQUEST_COMPLETED:
      return Object.assign({}, state, {
        form: {
          isFetching: false,
          isSubmitting: false,
          data: action.data || form.data,
          errors: action.errors,
        },
      });
    case FETCH_REQUEST_IN_PROGRESS:
      return Object.assign({}, state, {
        form: {
          isFetching: true,
          isSubmitting: false,
          data: form.data,
          errors: form.errors,
        },
      });
    case SAVE_REQUEST_IN_PROGRESS:
    case DELETE_REQUEST_IN_PROGRESS:
      return Object.assign({}, state, {
        form: {
          isFetching: false,
          isSubmitting: true,
          data: form.data,
          errors: form.errors,
        },
      });
    case DELETE_REQUEST_COMPLETED:
      return Object.assign({}, state, {
        form: {
          isFetching: false,
          isSubmitting: false,
          data: form.data,
          errors: action.errors,
        },
      });
    case REQUEST_PROP_CHANGED:
      form.data[action.key] = action.value;
      return Object.assign({}, state, {
        form: {
          isFetching: form.isFetching,
          isSubmitting: form.isSubmitting,
          data: form.data,
          errors: form.errors,
        },
      });
    case RESET_FORM_DATA:
      return Object.assign({}, state, {
        form: {
          isFetching: false,
          isSubmitting: false,
          data: { status: 'Open' },
          errors: null,
        },
      });
    case FETCH_REQUEST_LIST_IN_PROGRESS:
      return Object.assign({}, state, {
        list: {
          isFetching: true,
          isSubmitting: false,
          items: [],
          errors: list.errors,
          page: list.page,
        },
      });
    case FETCH_REQUEST_LIST_COMPLETED:
      return Object.assign({}, state, {
        list: {
          isFetching: false,
          isSubmitting: false,
          items: action.items || [],
          errors: action.errors,
          page: list.page,
        },
      });
    case UNEXPECTED_ERROR_HAPPENED:
      if (action.form) {
        return Object.assign({}, state, {
          form: {
            isFetching: form.isFetching,
            isSubmitting: form.isSubmitting,
            data: form.data,
            errors: action.errors,
          },
        });
      }
      return Object.assign({}, state, {
        list: {
          isFetching: list.isFetching,
          isSubmitting: list.isSubmitting,
          items: list.items,
          errors: action.errors,
          page: list.page,
        },
      });
    case CLOSE_REQUEST_SUCCESSFULLY:
      return Object.assign({}, state, {
        list: {
          isFetching: list.isFetching,
          isSubmitting: list.isSubmitting,
          items: list.items.map((item) => {
            if (item.requestId === action.request.requestId) {
              return action.request;
            }
            return item;
          }),
          errors: list.errors,
          page: list.page,
        },
      });
    default:
      return state;
  }
}
*/
