import { USER_HAS_AUTHENTICATED } from '../actions';

export default function app(state = { isAuthenticated: false }, action) {
  const { isAuthenticated } = action;
  switch (action.type) {
    case USER_HAS_AUTHENTICATED:
      return Object.assign({}, state, { isAuthenticated });
    default:
      return state;
  }
}
