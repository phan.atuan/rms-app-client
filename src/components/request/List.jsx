import React from 'react';
import { Row, Col, Pager, ButtonToolbar, ToggleButtonGroup, ToggleButton } from 'react-bootstrap';
import PropTypes from 'prop-types';
import 'react-notifications/lib/notifications.css';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import './List.css';
import RequestContainer from '../../containers/request/RequestContainer';
import CommentModal from '../../containers/request/comment/CommentModal';

function createNotification(type, message) {
  const title = 'Notification';
  const timeout = 3000; // 3000 ms
  return () => {
    switch (type) {
      case 'info':
        NotificationManager.info(message, title, timeout);
        break;
      case 'warning':
        NotificationManager.warning(message, title, timeout);
        break;
      case 'error':
        NotificationManager.error(message, title, timeout);
        break;
      case 'success':
      default:
        NotificationManager.success(message, title, timeout);
        break;
    }
  };
}
class RequestList extends React.Component {
  constructor(props) {
    super(props);
    this.handleGoToPage = this.handleGoToPage.bind(this);
    this.showComment = this.showComment.bind(this);
    this.handleHideComment = this.handleHideComment.bind(this);
    this.renderRequestList = this.renderRequestList.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
    this.state = {
      comment: {
        show: false,
        requestId: null,
      },
    };
  }

  showComment(requestId) {
    return this.setState({
      comment: {
        show: true,
        requestId,
      },
    });
  }

  handleHideComment() {
    return this.setState({
      comment: {
        show: false,
        requestId: null,
      },
    });
  }

  handleGoToPage(eventKey) {
    switch (eventKey) {
      case '1': // go to previous page
        this.props.goToPage('previous');
        break;
      case '2':
        this.props.goToPage('next');
        break;
      default:
        break;
    }
  }

  handleFilter(value) {
    this.props.filterByStatus(value);
  }

  renderRequestList(requests) {
    const rows = [];
    const renderRequest = (item, index) => {
      if (index % 3 === 0) {
        rows.push((<Row key={index} className="show-grid">{[]}</Row>));
      }
      const request = (
        <Col sm={12} md={4} key={item._id}>
          <RequestContainer
            showComment={this.showComment}
            createNotification={createNotification}
            requestId={item.requestId}
            shouldRefreshRequestList={this.props.shouldRefreshRequestList}
          />
        </Col>);
      rows[rows.length - 1].props.children.push(request);
    };
    requests.forEach(renderRequest);
    return rows;
  }

  render() {
    const { items: requests, isFetching } = this.props;
    let gridStyle = { opacity: 1 };
    if (isFetching) gridStyle = { opacity: 0.5 };
    return (
      <div style={gridStyle}>
        {isFetching && <Row className="show-grid"><Col sm={12}><h2>Loading.....</h2></Col></Row>}
        <Row className="show-grid">
          <Col sm={12} md={4}>
            <ButtonToolbar>
              <ToggleButtonGroup type="radio" name="options" defaultValue="Open" onChange={this.handleFilter}>
                <ToggleButton value="Open">Open</ToggleButton>
                <ToggleButton value="Close">Closed</ToggleButton>
                <ToggleButton value="Cancel">Cancelled</ToggleButton>
              </ToggleButtonGroup>
            </ButtonToolbar>
          </Col>
        </Row>
        {this.renderRequestList(requests)}
        <Row className="show-grid">
          <Pager onSelect={this.handleGoToPage}>
            <Pager.Item previous eventKey="1" disabled={this.props.previousDisabled}>
              &larr; Previous
            </Pager.Item>
            <Pager.Item next eventKey="2" disabled={this.props.nextDisabled}>Next &rarr;</Pager.Item>
          </Pager>
        </Row>
        <Row className="show-grid">
          {this.state.comment.show && <CommentModal
            show={this.state.comment.show}
            hideComment={this.handleHideComment}
            requestId={this.state.comment.requestId}
            createNotification={createNotification}
          />}
        </Row>
        <NotificationContainer />
      </div>
    );
  }
}

export default RequestList;

RequestList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.any).isRequired,
  previousDisabled: PropTypes.bool.isRequired,
  nextDisabled: PropTypes.bool.isRequired,
  isFetching: PropTypes.bool,
  filterByStatus: PropTypes.func.isRequired,
  goToPage: PropTypes.func.isRequired,
  shouldRefreshRequestList: PropTypes.func.isRequired,
};

RequestList.defaultProps = {
  isFetching: false,
};

