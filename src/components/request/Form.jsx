import React from 'react';
import { Form, FormGroup, FormControl, Col, ControlLabel, Button, HelpBlock, ListGroup, ListGroupItem } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Prompt } from 'react-router-dom';
import moment from 'moment';
import LoaderButton from '../loader-button/LoaderButton';

const FieldGroup = ({ id,
  label,
  help,
  children,
  ...rests
}) =>
  (
    <FormGroup controlId={id}>
      <Col componentClass={ControlLabel} xsPush={1} xs={11} sm={2}>
        {label}
      </Col>
      <Col xsPush={1} xs={11} sm={6}>
        <FormControl {...rests}>
          {children}
        </FormControl>
        {help && <HelpBlock>{help}</HelpBlock>}
      </Col>
    </FormGroup>
  );

function renderComment(item, index) {
  return (<ListGroupItem key={index}>{moment(item.createdDate).format('MMMM Do YYYY')}<br />{item.text}</ListGroupItem>);
}
export default class ResourceRequestForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.validateForm = this.validateForm.bind(this);
    this.state = {
      isBlocking: false,
      isLoadingComment: false,
      comments: [],
    };
  }
  /*
  componentWillMount() {
    if (this.props.id !== 'create') {
      this.props.fetchRequestById(this.props.id);
      this.setState(prevState => Object.assign({}, prevState, { isLoadingComment: true, comments: [] }));
      this.props.fetchCommentList(this.props.id)
        .then(response => this.setState(prevState => Object.assign({}, prevState, { isLoadingComment: false, comments: response.items })));
    } else {
      this.props.reset();
    }
  }
  */
  componentWillReceiveProps(nextProps) {
    if (nextProps.id === 'create' && !!nextProps.data.requestId) {
      this.props.reset();
    } else if (nextProps.id !== this.props.id) {
      this.props.fetchRequestById(nextProps.id);
    }
  }
  /*
  shouldComponentUpdate(nextProps) {
    if (nextProps.id === 'create' && !!nextProps.data.requestId) {
      return false;
    }
    return true;
  }
  */
  validateForm() {
    const { request } = this.props.data;
    return request.accountName && this.props.data.request.accountName.length > 0 &&
      request.resourceType && request.resourceType.length > 0 &&
      request.resourceRate && !isNaN(request.resourceRate) &&
      request.quantity && !isNaN(request.quantity);
  }

  handleChange(event) {
    this.setState({ isBlocking: true });
    this.props.handleChange(event);
  }

  handleSubmit(e) {
    e.preventDefault();
    this.setState({ isBlocking: false });
    const isDelete = (this.props.location.state &&
      this.props.location.state.isDelete);
    this.props.handleSubmit(isDelete);
  }

  render() {
    const { isBlocking } = this.state;
    const isDisabled = (this.props.location.state &&
      this.props.location.state.isDelete);
    const {
      accountName,
      resourceType,
      resourceRate,
      quantity,
      submissionDate,
      tentativeStartDate,
      fulfilmentDate,
      status,
    } = this.props.data.request;

    const { errors, isSubmitting } = this.props.data;
    const {
      items: comments,
      isLoading: isLoadingComment,
      errors: commentErrors,
    } = this.props.data.comment;
    const button = this.props.id !== 'create' ? 'Update' : 'Create New';

    return (
      <div>
        <Form horizontal onSubmit={this.handleSubmit}>
          <Prompt
            when={isBlocking}
            message={location => (
            `Are you sure you want to go to ${location.pathname}`)}
          />
          {
            (errors.system || commentErrors.system) &&
            <FormGroup>
              <Col xsPush={1} xs={11} smOffset={2} sm={6}>
                <div key="danger" className="alert alert-danger">
                  {errors.system || commentErrors.system}
                </div>
              </Col>
            </FormGroup>
          }
          {
            (this.props.location.state &&
              this.props.location.state.isDelete) &&
              <FormGroup>
                <Col componentClass={ControlLabel} xsPush={1} xs={11} sm={2}>
                  {' '}
                </Col>
                <Col xsPush={1} xs={11} sm={6}>
                  <div key="danger" className="alert alert-danger">
                    Are you sure you want to delete the below request?
                  </div>
                </Col>
              </FormGroup>
          }
          <FieldGroup
            id="accountName"
            label="Account Name:"
            type="text"
            placeholder="Account Name"
            disabled={isDisabled}
            onChange={this.handleChange}
            value={accountName || ''}
            help={errors.accountName}
          />
          <FieldGroup
            id="resourceType"
            label="Resource Type:"
            type="text"
            placeholder="Resource Type"
            disabled={isDisabled}
            onChange={this.handleChange}
            value={resourceType || ''}
            help={errors.resourceType}
          />
          <FieldGroup
            id="resourceRate"
            label="Resource Rate:"
            type="text"
            placeholder="Resource Rate"
            disabled={isDisabled}
            onChange={this.handleChange}
            value={resourceRate || 0}
            help={errors.resourceRate}
          />
          <FieldGroup
            id="quantity"
            label="Resource Quantity:"
            type="text"
            placeholder="Resource Quantity"
            disabled={isDisabled}
            onChange={this.handleChange}
            value={quantity || 0}
            help={errors.quantity}
          />
          <FieldGroup
            id="submissionDate"
            label="Submission Date:"
            type="date"
            placeholder="Submission Date"
            disabled={isDisabled}
            onChange={this.handleChange}
            value={submissionDate || ''}
            help={errors.submissionDate}
          />
          <FieldGroup
            id="tentativeStartDate"
            label="Start Date:"
            type="date"
            placeholder="Tentative Start Date"
            disabled={isDisabled}
            onChange={this.handleChange}
            value={tentativeStartDate || ''}
            help={errors.tentativeStartDate}
          />
          <FieldGroup
            id="fulfilmentDate"
            label="Fulfilment Date:"
            type="date"
            placeholder="Fulfilment Date"
            disabled={isDisabled}
            onChange={this.handleChange}
            value={fulfilmentDate || ''}
            help={errors.fulfilmentDate}
          />
          <FieldGroup
            id="status"
            label="Status:"
            componentClass="select"
            placeholder="Status"
            disabled={isDisabled}
            onChange={this.handleChange}
            value={status || 'Open'}
            help={errors.status}
          >
            <option value="Open">Open</option>
            <option value="Close">Close</option>
            <option value="Cancel">Cancel</option>
          </FieldGroup>
          <FormGroup>
            <Col xsPush={1} xs={11} smOffset={2} sm={6}>
              <Button onClick={this.props.history.goBack}>
                Back
              </Button>
              &nbsp;
              <LoaderButton
                bsStyle="primary"
                disabled={!this.validateForm()}
                type="submit"
                isLoading={isSubmitting}
                text={(this.props.location.state && this.props.location.state.isDelete) ? 'Delete' : button}
                loadingText={(this.props.location.state && this.props.location.state.isDelete) ? 'Deleting...' : 'Saving...'}
              />
            </Col>
          </FormGroup>
        </Form>
        <p>Comments:</p>
        { isLoadingComment ?
          (<ListGroup><ListGroupItem>Loading comments ....</ListGroupItem></ListGroup>)
          :
          <ListGroup>
            {comments.length > 0 && !commentErrors.system
            ?
            comments.map(renderComment)
            :
            (<ListGroupItem>There is no comment</ListGroupItem>)}
          </ListGroup>
        }
      </div>
    );
  }
}

ResourceRequestForm.propTypes = {
  id: PropTypes.string.isRequired,
  data: PropTypes.object.isRequired,
  // errors: PropTypes.object,
  history: PropTypes.object.isRequired,
  // isSubmitting: PropTypes.bool,
  // fetchRequestById: PropTypes.func.isRequired,
  // fetchCommentList: PropTypes.func.isRequired,
  // reset: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

ResourceRequestForm.defaultProps = {
};

