import React from 'react';
import { Panel, ListGroup, ListGroupItem, InputGroup, FormControl, DropdownButton, MenuItem } from 'react-bootstrap';
import PropTypes from 'prop-types';
import moment from 'moment';

export default class Index extends React.Component {
  constructor(props) {
    super(props);
    this.handleSelect = this.handleSelect.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      comment: '',
    };
  }

  /*
  componentWillMount() {
    this.props.fetchTheMostRecentComment(this.props.data.requestId)
      .then((response) => {
        this.setState({ comment: response.items.length > 0 ? response.items[0].text : '' });
        console.log(`get comment of ${this.props.data.accountName} ${this.props.data.requestId}: ${response.items.length > 0 ? response.items[0].text : ''}`);
      })
      .catch(error => this.props.createNotification('error', error.message)());
  }
  */

  handleChange(event) {
    const { target } = event;
    this.setState(Object.assign({}, this.state, { [target.id]: target.value }));
  }

  handleSelect(eventKey) {
    const { requestId, submissionDate } = this.props.data;
    switch (eventKey) {
      case '1':
        this.props.addRequestComment(requestId, this.state.text)
          .then(() => this.setState({ text: '' }));
        break;
      case '2': /* Fulfill the request so close it */
        this.props.closeRequestWithComment(requestId, this.state.text, 'Close', submissionDate)
          .then((isRefresh) => { if (!isRefresh) { this.setState({ text: '' }); } });
        break;
      case '3': /* Fail to fulfill the request so cancel it */
        this.props.closeRequestWithComment(requestId, this.state.text, 'Cancel', submissionDate)
          .then((isRefresh) => { if (!isRefresh) { this.setState({ text: '' }); } });
        break;
      case '4': // Show all comments
        this.props.showComment(requestId);
        break;
      case '5': // Edit the request
        this.props.history.push(`request/${requestId}`);
        break;
      case '6':
        // this.props.history.location.state = { isDelete: true };
        this.props.history.push(`request/${requestId}`, { isDelete: true });
        break;
      default:
        alert(`${eventKey} is clicked!`);
    }
  }

  render() {
    const {
      accountName, resourceType, resourceRate, quantity,
      submissionDate, tentativeStartDate, fulfilmentDate, status, comment,
    } = this.props.data;

    const age = -1 * moment(submissionDate).diff(moment(), 'days');
    return (
      <Panel header={submissionDate ? `${accountName.toUpperCase()} - ${moment(submissionDate).format('MMMM Do YYYY')}` : accountName.toUpperCase()} bsStyle="primary">
        <ListGroup>
          <ListGroupItem>Account: {accountName}</ListGroupItem>
          <ListGroupItem>Position: {resourceType}</ListGroupItem>
          <ListGroupItem>Rate: {resourceRate}</ListGroupItem>
          <ListGroupItem>Quantity: {quantity}</ListGroupItem>
          <ListGroupItem>Age: { age < 14 ? `${age} days ago` : <span style={{ color: 'red' }}>{age} days ago</span>}</ListGroupItem>
          <ListGroupItem>Tentative Start Date: { (tentativeStartDate) ? moment(tentativeStartDate).format('MMMM Do YYYY') : <span style={{ color: 'red' }}>TBD</span>}</ListGroupItem>
          <ListGroupItem>Fulfilment Date: { (fulfilmentDate) ? moment(fulfilmentDate).format('MMMM Do YYYY') : <span style={{ color: 'red' }}>TBD</span>}</ListGroupItem>
          <ListGroupItem>Status: {status}</ListGroupItem>
          <ListGroupItem>Comment:<br />{comment}</ListGroupItem>
        </ListGroup>
        <InputGroup>
          <FormControl id="text" type="text" placeholder="Comment" onChange={this.handleChange} value={this.state.text || ''} />
          <DropdownButton
            componentClass={InputGroup.Button}
            id="input-dropdown-addon"
            title="Action"
            onSelect={eventKey => this.handleSelect(eventKey)}
          >
            <MenuItem eventKey="1">
              <span className="glyphicon glyphicon-plus" aria-hidden="true">&nbsp;Comment</span>
            </MenuItem>
            <MenuItem eventKey="2">
              <span className="glyphicon glyphicon-ok-circle" aria-hidden="true">&nbsp;Close</span>
            </MenuItem>
            <MenuItem eventKey="3">
              <span className="glyphicon glyphicon-remove-circle" aria-hidden="true">
                &nbsp;Cancel
              </span>
            </MenuItem>
            <MenuItem eventKey="4">
              <span className="glyphicon glyphicon-eye-open" aria-hidden="true">
                &nbsp;Comments
              </span>
            </MenuItem>
            <MenuItem eventKey="5">
              <span className="glyphicon glyphicon-edit" aria-hidden="true">&nbsp;Edit</span>
            </MenuItem>
            <MenuItem eventKey="6">
              <span className="glyphicon glyphicon-trash" aria-hidden="true">&nbsp;Remove</span>
            </MenuItem>
          </DropdownButton>
        </InputGroup>
      </Panel>);
  }
}

Index.propTypes = {
  data: PropTypes.shape({
    userId: PropTypes.string,
    requestId: PropTypes.string,
    accountName: PropTypes.string,
    resourceType: PropTypes.string,
    resourceRate: PropTypes.number,
    quantity: PropTypes.number,
    submissionDate: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.object,
    ]),
    tentativeStartDate: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.object,
    ]),
    fulfilmentDate: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.object,
    ]),
    status: PropTypes.oneOf(['Open', 'Close', 'Cancel']),
    comment: PropTypes.string,
    createdAt: PropTypes.number,
  }).isRequired,
  addRequestComment: PropTypes.func.isRequired,
  closeRequestWithComment: PropTypes.func.isRequired,
  showComment: PropTypes.func.isRequired,
  // fetchTheMostRecentComment: PropTypes.func.isRequired,
  createNotification: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};
