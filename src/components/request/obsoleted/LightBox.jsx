import React from 'react';
import PropTypes from 'prop-types';

export default class Lightbox extends React.Component {
  constructor(props) {
    super(props);
    this.openLightbox = this.openLightbox.bind(this);
    this.closeLightbox = this.closeLightbox.bind(this);
    this.setLightboxState = this.setLightboxState.bind(this);
    this.state = { display: false };
  }

  componentWillMount() {
    if (this.props.data) this.setState(this.props.data);
  }

  setLightboxState(obj) {
    this.setState(obj);
  }

  closeLightbox() {
    this.setState({ display: false });
  }

  openLightbox() {
    this.setState({ display: true });
  }

  render() {
    const that = this;
    let i = 0;
    const childrenWithProps = React.Children.map(this.props.children, (child) => {
      i += 1;
      const childProps = {
        openLightbox: that.openLightbox,
        closeLightbox: that.closeLightbox,
        setLightboxState: that.setLightboxState,
        key: i,
      };
      Object.entries(that.state).forEach((item) => {
        const [key, value] = item;
        childProps[key] = value;
      });
      /*
      for (let j in that.state) {
        childProps[j] = that.state[j];
      }
      */
      const childWithProps = React.cloneElement(child, childProps);
      return childWithProps;
    });

    return (
      <div>
        {childrenWithProps}
      </div>
    );
  }
}

Lightbox.propTypes = {
  data: PropTypes.objectOf(PropTypes.any),
  display: PropTypes.bool,
};

Lightbox.defaultProps = {
  data: {},
  display: false,
};

