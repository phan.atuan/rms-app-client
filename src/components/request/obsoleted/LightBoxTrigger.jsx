import React from 'react';

export default class LightboxTrigger extends React.Component {
  render() {
    const that = this;
    const { children, ...rest } = that.props;
    const childrenWithProps = React.Children.map(that.props.children, (child) => {
      const childWithProps = React.cloneElement(child, { onClick: that.props.openLightbox, ...rest });
      return childWithProps;
    });
    return childrenWithProps[0];
  }
}
