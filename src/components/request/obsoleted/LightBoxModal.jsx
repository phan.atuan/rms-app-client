import React from 'react';
import './ReactLightBox.css';

/* CSS from http://stackoverflow.com/questions/19064987/html-css-popup-div-on-text-click
  and http://stackoverflow.com/questions/10019797/pure-css-close-button
  source code from https://github.com/howtomakeaturn/React-Lightbox/blob/master/react-lightbox.jsx
*/

export default class LightboxModal extends React.Component {
  constructor(props) {
    super(props);
    this.keydownHandler = this.keydownHandler.bind(this);
  }

  componentDidMount() {
    document.addEventListener('keydown', this.keydownHandler);
  }

  keydownHandler(e) {
    if ((this.props.display) && (e.keyCode === 27)) {
      this.props.closeLightbox();
    }
  }
  render() {
    const that = this;
    const {
      children,
      display,
      closeLightbox,
      ...rest
    } = that.props;
    const childrenWithProps = React.Children.map(that.props.children, (child) => {
      const childWithProps = React.cloneElement(child, { ...rest });
      return childWithProps;
    });

    if (this.props.display) {
      return (
        <div>
          <div
            style={Object.assign({},this.blackOverlayStyles,!!this.props.blackOverlayStyles ? this.props.blackOverlayStyles: {})}
            onClick={this.props.closeLightbox} />
          <div style={Object.assign({},this.whiteContentStyles,!!this.props.whiteContentStyles? this.props.whiteContentStyles: {})}>
              <a style={Object.assign({},this.closeTagStyles,!!this.props.closeTagStyles? this.props.closeTagStyles: {})} onClick={this.props.closeLightbox}>&times;</a>
              {childrenWithProps}
          </div>
        </div>
      );
    }
    return (<div />);
  }
}

