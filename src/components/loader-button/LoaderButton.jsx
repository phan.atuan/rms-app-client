import React from 'react';
import { Button, Glyphicon } from 'react-bootstrap';
import PropTypes from 'prop-types';
import './LoaderButton.css';

export default function LoaderButton({
  className,
  text,
  loadingText,
  isLoading,
  disabled,
  ...props
}) {
  return (
    <Button
      className={`LoaderButton ${className}`}
      disabled={disabled || isLoading}
      {...props}
    >
      { isLoading && <Glyphicon glyph="refresh" className="spinning" />}
      { isLoading ? loadingText : text}
    </Button>
  );
}

LoaderButton.propTypes = {
  className: PropTypes.string,
  text: PropTypes.string.isRequired,
  loadingText: PropTypes.string.isRequired,
  isLoading: PropTypes.bool.isRequired,
  disabled: PropTypes.bool.isRequired,
};

LoaderButton.defaultProps = {
  className: '',
};

