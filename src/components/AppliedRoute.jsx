import React from 'react';
import { Route } from 'react-router-dom';
import PropTypes from 'prop-types';

export default function AppliedRoute({ component: C, ...rest }) {
  return (
    <Route {...rest} render={props => <C {...props} />} />
  );
}
/*
const mapStateToProps = (state, ownProps) => {
    return {
        isAuthenticated: state.app.isAuthenticated,
    };
};

export default connect(mapStateToProps, null)(AppliedRoute);
*/

AppliedRoute.propTypes = {
  component: PropTypes.oneOfType([PropTypes.instanceOf(React.Component),
    PropTypes.func]).isRequired,
};
