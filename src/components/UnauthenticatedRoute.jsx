import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

function querystring(name, url = window.location.href) {
  const qname = name.replace(/[[]]/g, '\\$&');

  const regex = new RegExp(`[?&] ${qname} (=([^&#]*)|&|#|$)`, 'i');
  const results = regex.exec(url);

  if (!results) {
    return null;
  }
  if (!results[2]) {
    return '';
  }

  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function UnauthenticatedRoute({ component: C, isAuthenticated, ...rest }) {
  const redirect = querystring('redirect');
  return (
    <Route
      {...rest}
      render={(props) => {
      if (!isAuthenticated) return (<C {...props} />);
      return (<Redirect to={redirect === '' || redirect === null ? '/' : redirect} />);
    }}
    />
  );
}

const mapStateToProps = state =>
  Object.assign({}, {
    isAuthenticated: state.app.isAuthenticated,
  });

export default connect(mapStateToProps, null)(UnauthenticatedRoute);

UnauthenticatedRoute.propTypes = {
  component: PropTypes.oneOfType([PropTypes.instanceOf(React.Component),
    PropTypes.func]).isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
};
