import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

function AuthenticatedRoute({ component: C, isAuthenticated, ...rest }) {
  return (
    <Route
      {...rest}
      render={(props) => {
        if (isAuthenticated) return (<C {...props} />);
        return (<Redirect to={`/login?redirect=${props.location.pathname}${props.location.search}`} />);
      }
    }
    />
  );
}

const mapStateToProps = state =>
  Object.assign({}, {
    isAuthenticated: state.app.isAuthenticated,
  });

export default connect(mapStateToProps, null)(AuthenticatedRoute);

AuthenticatedRoute.propTypes = {
  component: PropTypes.oneOfType([PropTypes.instanceOf(React.Component),
    PropTypes.func]).isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  location: PropTypes.objectOf(PropTypes.any).isRequired,
};
