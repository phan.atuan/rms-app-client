import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './containers/home/Home';
import NotFound from './containers/notfound/NotFound';
import Login from './containers/login/Login';
import RequestForm from './containers/request/FormContainer';
import AppliedRoute from './components/AppliedRoute';
import AuthenticatedRoute from './components/AuthenticatedRoute';
import UnauthenticatedRoute from './components/UnauthenticatedRoute';
import asyncComponent from './components/AsyncComponent';

const AsyncSignup = asyncComponent(() => import("./containers/signup/Signup"));

export default ({ childProps }) => {
    return (
        <Switch>
             <AppliedRoute path="/" exact component={Home}/>
             <UnauthenticatedRoute path="/login" exact component={Login}/>
             <UnauthenticatedRoute path="/signup" exact component={AsyncSignup} />
             <AuthenticatedRoute path="/request/:id" exact component={RequestForm} />
             <Route component={NotFound}/>
        </Switch>
    )
}