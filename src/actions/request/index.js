import { API } from 'aws-amplify';
import moment from 'moment';
import config from '../../aws-exports';

const SYSTEM_ERROR_MESSAGE = 'Unexpected error happened, please contact system administrator for assistant';

export function FetchRequestById(id) {
  const { name, paths } = config.aws_cloud_logic_custom[0];
  const path = paths[1].replace('{id}', encodeURI(id));
  // send GET request to api endpoint /api/requests/{id}
  return API.get(name, path)
    .catch(() => { throw new Error(SYSTEM_ERROR_MESSAGE); });
}

export function FetchRequestList(status = 'All', token, pageSize) {
  const { name, paths } = config.aws_cloud_logic_custom[0];
  const queryStringParameters = token ? `?token=${encodeURIComponent(token)}&limit=${pageSize}` : `?limit=${pageSize}`;
  const path = paths[4].replace('{status}', encodeURI(status)).concat(queryStringParameters);
  // send GET request to api endpoint /api/requests/status/{status}?token=<token>?limit=<pageSize>
  return API.get(name, path)
    .catch(() => { throw new Error(SYSTEM_ERROR_MESSAGE); });
}

export function SaveRequest(body) {
  const myInit = {
    // response: true, // OPTIONAL (return entire response object instead of response.data)
    body,
  };
  const { name, paths } = config.aws_cloud_logic_custom[0];
  const action = body.requestId ? 'put' : 'post';
  const path = body.requestId ? paths[0].concat(`/${body.requestId}`) : paths[0];
  // to create new Request: POST a request to /api/requests
  // to update an existing request: PUT a request to /api/requests/{id}
  return API[action](name, path, myInit)
    .catch(() => { throw new Error(SYSTEM_ERROR_MESSAGE); });
}

export function DeleteRequest(id) {
  const { name, paths } = config.aws_cloud_logic_custom[0];
  const path = paths[1].replace('{id}', encodeURI(id));
  // send DELETE request to /api/requests/{id}
  return API.del(name, path)
    .catch(() => { throw new Error(SYSTEM_ERROR_MESSAGE); });
}

export function CloseRequestWithComment(requestId, text, status, submissionDate) {
  const { name, paths } = config.aws_cloud_logic_custom[0];
  const path = paths[1].replace('{id}', encodeURI(requestId));
  const body = {
    status,
    text,
    submissionDate: moment(submissionDate).format('YYYY-MM-DD'),
    fulfilmentDate: moment().format('YYYY-MM-DD'),
  };
  const myInit = {
    // response: true, // OPTIONAL (return entire response object instead of response.data)
    body,
  };
  // POST a request to /api/requests/{id}
  return API.post(name, path, myInit)
    .catch(() => { throw new Error(SYSTEM_ERROR_MESSAGE); });
}
