import { API } from 'aws-amplify';
import config from '../../../aws-exports';

const SYSTEM_ERROR_MESSAGE = 'Unexpected error happened, please contact system administrator for assistant';

export function AddRequestComment(requestId, text) {
  const { name, paths } = config.aws_cloud_logic_custom[0];
  const path = paths[2];
  const body = {
    requestId,
    text,
  };
  const myInit = {
    // response: true, // OPTIONAL (return entire response object instead of response.data)
    body,
  };
  // POST a request to /api/request/comments
  return API.post(name, path, myInit)
    .catch(() => { throw new Error(SYSTEM_ERROR_MESSAGE); });
}

export function FetchRequestComment(requestId, count = 0) {
  const { name, paths } = config.aws_cloud_logic_custom[0];
  const path = paths[3].replace('{id}', encodeURI(requestId)).concat(`?count=${count}`);
  // send GET request to /api/request/comments/{id}?count=<top N comment on the request>
  return API.get(name, path)
    .catch(() => { throw new Error(SYSTEM_ERROR_MESSAGE); });
}

