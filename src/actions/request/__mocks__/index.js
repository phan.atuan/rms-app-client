const SYSTEM_ERROR_MESSAGE = 'Unexpected error happened, please contact system administrator for assistant';

const requests = new Map();

requests.set('41ce22a0-6c72-11e8-a942-15a5a329ef14', {
  userId: 'ap-southeast-2:5f71c4ba-6787-4f93-9d72-541c52493d27',
  requestId: '41ce22a0-6c72-11e8-a942-15a5a329ef14',
  accountName: 'Schiavello',
  resourceType: 'M-files developers',
  resourceRate: 240,
  quantity: 1,
  submissionDate: 1528588800000,
  tentativeStartDate: 1529020800000,
  fulfilmentDate: 1528588800000,
  status: 'Open',
});

requests.set('d24fe8f0-6d25-11e8-bb33-5fdc80fe2e60', {
  userId: 'ap-southeast-2:5f71c4ba-6787-4f93-9d72-541c52493d27',
  requestId: 'd24fe8f0-6d25-11e8-bb33-5fdc80fe2e60',
  accountName: 'Study Group',
  resourceType: '.NET developer',
  resourceRate: 180,
  quantity: 5,
  submissionDate: 1527775200000,
  tentativeStartDate: 1529503200000,
  fulfilmentDate: null,
  status: 'Open',
});

requests.set('c8b36e20-6d25-11e8-bb33-5fdc80fe2e60', {
  userId: 'ap-southeast-2:5f71c4ba-6787-4f93-9d72-541c52493d27',
  requestId: 'c8b36e20-6d25-11e8-bb33-5fdc80fe2e60',
  accountName: 'Study Group',
  resourceType: 'Automation test engineer',
  resourceRate: 185,
  quantity: 5,
  submissionDate: 1527775200000,
  tentativeStartDate: 1529503200000,
  fulfilmentDate: null,
  status: 'Open',
});

export function FetchRequestById(id) {
  const item = requests.get(id);
  return new Promise(resolve =>
    process.nextTick(() => resolve({
      item,
      status: true,
    })));
}

export function FetchRequestList(status = 'All', token, pageSize) {
  const items = [];
  requests.forEach(value => items.push(value));

  return new Promise(resolve =>
    process.nextTick(() => resolve({
      items,
      token: 'eyJQSyI6ImFwLXNvdXRoZWFzdC0yOjVmNzFjNGJhLTY3ODctNGY5My05ZDcyLTU0MWM1MjQ5M2QyNyIsIkxTSTEtU0siOiJPcGVuIzIwMTgtMDYtMDEiLCJTSyI6ImM4YjM2ZTIwLTZkMjUtMTFlOC1iYjMzLTVmZGM4MGZlMmU2MCJ9',
      status: true,
    })));
}

export function SaveRequest(body) {
}

export function DeleteRequest(id) {
}

export function CloseRequestWithComment(requestId, text, status, submissionDate) {
}
