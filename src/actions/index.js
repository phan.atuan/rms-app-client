import { Auth } from 'aws-amplify';

export const USER_HAS_AUTHENTICATED = 'USER_HAS_AUTHENTICATED';

/**
 * flag/unflag isAuthenticated
 */
export function UseHasAuthenticated(isAuthenticated) {
  return {
    type: USER_HAS_AUTHENTICATED,
    isAuthenticated,
  };
}

export function GetCurrentSession() {
  return dispatch =>
    Auth.currentSession().then(() => dispatch(UseHasAuthenticated(true)));
}

export function SignOut() {
  return dispatch =>
    Auth.signOut().then(() => dispatch(UseHasAuthenticated(false)));
}

export function SignIn(username, password) {
  return dispatch =>
    Auth.signIn(username, password)
      .then(() => dispatch(UseHasAuthenticated(true)));
}


export function SignUp(user) {
  return () =>
    Auth.signUp(user);
}

export function ConfirmSignUp(username, confirmationCode) {
  return () =>
    Auth.confirmSignUp(username, confirmationCode);
}

