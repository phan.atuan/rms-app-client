export default {
  api: {
    request: {
      path: '/api/requests',
      endpoint: 'api-gateway-endpoint',
      region: 'aws-gateway-region',
    },
  },
};
