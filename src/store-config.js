import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import app from './reducers';

const loggerMiddleware = createLogger();

const initialState = {
  app: {
    isAuthenticated: false,
  },
};

const rootReducer = combineReducers({
  app,
});

export default function storeConfig() {
  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(
      thunkMiddleware,
      loggerMiddleware,
    ),
  );
}
