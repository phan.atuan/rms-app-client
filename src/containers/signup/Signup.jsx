import React from 'react';
import { HelpBlock, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import './Signup.css';
import LoaderButton from '../../components/loader-button/LoaderButton';
import { SignIn, SignUp, ConfirmSignUp } from '../../actions';

class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      username: '',
      email: '',
      password: '',
      confirmPassword: '',
      confirmationCode: '',
      newUser: null,
    };
    this.handleChange = this.handleChange.bind(this);
    this.validateForm = this.validateForm.bind(this);
    this.validateConfirmationForm = this.validateConfirmationForm.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleConfirmationSubmit = this.handleConfirmationSubmit.bind(this);
    this.renderConfirmationForm = this.renderConfirmationForm.bind(this);
    this.renderForm = this.renderForm.bind(this);
  }

  handleChange(e) {
    this.setState({
      [e.target.id]: e.target.value,
    });
  }

  validateForm() {
    return (
      this.state.username.length > 0 &&
      this.state.email.length > 0 &&
      this.state.password.length > 0 &&
      this.state.password === this.state.confirmPassword
    );
  }

  validateConfirmationForm() {
    return this.state.confirmationCode.length > 0;
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({ isLoading: true });
    const user = {
      username: this.state.username,
      password: this.state.password,
      attributes: {
        email: this.state.email,
        'custom:isActive': '1',
        'custom:roles': ['employee', 'manager'].toString(),
        'custom:timeCreated': Date.now().toString(),
      },
    };
    this.props.signUp(user)
      .then(newUser => this.setState({ newUser }))
      .catch(error => alert(`Signup.js: ${error}`));
    this.setState({ isLoading: false });
  }

  handleConfirmationSubmit(e) {
    e.preventDefault();
    this.setState({ isLoading: true });

    this.props.confirmSignUp(this.state.username, this.state.confirmationCode)
      .then(() => this.props.signIn(this.state.username, this.state.password))
      .catch((error) => {
        alert(error.message);
        this.setState({ isLoading: false });
      });
  }

  renderConfirmationForm() {
    return (
      <form onSubmit={this.handleConfirmationSubmit}>
        <FormGroup controlId="confirmationCode" bsSize="large">
          <ControlLabel>Confirmation Code</ControlLabel>
          <FormControl
            autoFocus
            type="tel"
            value={this.state.confirmationCode}
            onChange={this.handleChange}
          />
          <HelpBlock>Please check your email for the code</HelpBlock>
        </FormGroup>
        <LoaderButton
          block
          bsSize="large"
          isLoading={this.state.isLoading}
          disabled={!this.validateConfirmationForm()}
          type="submit"
          text="Verify"
          loadingText="Verifying"
        />
      </form>
    );
  }

  renderForm() {
    return (
      <form onSubmit={this.handleSubmit}>
        <FormGroup controlId="username" bsSize="large">
          <ControlLabel>Username</ControlLabel>
          <FormControl
            autoFocus
            type="text"
            onChange={this.handleChange}
            value={this.state.username}
          />
          <FormGroup controlId="email" bsSize="large">
            <ControlLabel>Email</ControlLabel>
            <FormControl
              autoFocus
              type="email"
              onChange={this.handleChange}
              value={this.state.email}
            />
          </FormGroup>
          <FormGroup controlId="password" bsSize="large">
            <ControlLabel>Password</ControlLabel>
            <FormControl
              type="password"
              onChange={this.handleChange}
              value={this.state.password}
            />
          </FormGroup>
          <FormGroup controlId="confirmPassword" bsSize="large">
            <ControlLabel>Confirm Password</ControlLabel>
            <FormControl
              type="password"
              onChange={this.handleChange}
              value={this.state.confirmPassword}
            />
          </FormGroup>
          <LoaderButton
            block
            bsSize="large"
            disabled={!this.validateForm()}
            type="submit"
            isLoading={this.state.isLoading}
            text="Signup"
            loadingText="Signing up…"
          />
        </FormGroup>
      </form>
    );
  }

  render() {
    return (
      <div className="Signup">
        {this.state.newUser === null
          ? this.renderForm()
          : this.renderConfirmationForm()}
      </div>
    );
  }
}


Signup.propTypes = {
  signUp: PropTypes.func.isRequired,
  signIn: PropTypes.func.isRequired,
  confirmSignUp: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch =>
  Object.assign({}, {
    signIn: (username, passsword) => dispatch(SignIn(username, passsword)),
    signUp: user => dispatch(SignUp(user)),
    confirmSignUp: (username, confirmationCode) =>
      dispatch(ConfirmSignUp(username, confirmationCode)),
  });


export default connect(null, mapDispatchToProps)(Signup);
