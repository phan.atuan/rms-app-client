import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './Home.css';
import ListContainer from '../request/ListContainer';

export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static renderLander() {
    return (
      <div className="lander">
        <h1>Request Management</h1>
        <p>A simple app to manage resource requests</p>
      </div>
    );
  }

  static renderHome() {
    return (
      <ListContainer />
    );
  }

  render() {
    return (
      <div className="Home">
        {
          this.props.isAuthenticated
          ? Home.renderHome()
          : Home.renderLander()
        }
      </div>
    );
  }
}

Home.propTypes = {
  isAuthenticated: PropTypes.bool,
};

Home.defaultProps = {
  isAuthenticated: false,
};

const mapStateToProps = state =>
  Object.assign({}, {
    isAuthenticated: state.app.isAuthenticated,
  });


export default connect(mapStateToProps)(Home);

