import React from 'react';
import { MemoryRouter } from 'react-router';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import { Provider } from 'react-redux';
import App from '../app/App';
import storeConfig from '../../store-config';

/* ****************************************************************************************
 *  aws-amplify module is mocked in config/__mocks__/aws-amplify
 * see the detailed guideline at https://facebook.github.io/jest/docs/en/manual-mocks.html
 * ************************************************************************************** */
const Amplify = require('aws-amplify');

let _promise;
let store;
let wrapper;

describe('Signup Component', () => {
  beforeEach(() => {
    store = storeConfig();
    /* **********************************************************************
     * overrite Auth.currentSession mock to simulate unauthenticated session
     ********************************************************************* */
    Amplify.Auth.currentSession = jest.fn().mockImplementation(() => {
      // _promise = Promise.reject (new Error('No current user'));
      _promise = new Promise((resolve, reject) => process.nextTick(() => reject(new Error('No current user'))));
      return _promise;
    });

    Amplify.Auth.currentSession.mockClear();
    Amplify.Auth.signIn.mockClear();
    Amplify.Auth.signUp.mockClear();
    Amplify.Auth.confirmSignUp.mockClear();
  });

  afterEach(() => { });

  it('snapshot matches correctly', (done) => {
    const _wrapper = mount(<Provider store={store}>
      <MemoryRouter initialEntries={[{ pathname: '/signup', key: 'testKey' }]}>
        <App />
      </MemoryRouter>
    </Provider>);

    _promise.catch(() => {
      _wrapper.update();
      expect(toJson(_wrapper)).toMatchSnapshot();
      done();
    });
  });

  it('call Auth.signUp correctly', (done) => {
    wrapper = mount(<Provider store={store}>
      <MemoryRouter initialEntries={[{ pathname: '/signup', key: 'testKey' }]}>
        <App />
      </MemoryRouter>
    </Provider>);
    _promise.catch(() => {
      wrapper.update();
      wrapper.find('form').simulate('submit');
      expect(Amplify.Auth.signUp.mock.calls).toHaveLength(1);
      // call Auth.signIn with 1 argument which is user object
      expect(Amplify.Auth.signUp.mock.calls[0]).toHaveLength(1);
      Amplify.Auth_signUp_promise.then(() => {
        /* ******************************************************
        * After user signed up, user is asked for confirmation.
        ****************************************************** */
        wrapper.update();
        // console.log (wrapper.debug ());
        expect(wrapper.find('#confirmationCode').exists()).toBeTruthy();
        done();
      });
    });
  });

  it('call Auth.confirmSignUp correctly', (done) => {
    /* ******************************************************
      * Following it("call Auth.signUp correctly")
      * After user signed up, user is asked for confirmation.
      ***************************************************** */
    // console.log (wrapper.debug ());
    wrapper.find('form').simulate('submit');
    Amplify.Auth_confirmSignUp_promise.then(() => {
    /* ******************************************************
    * After user send confirmation code, user is signed in
    ******************************************************* */
      wrapper.update();
      // console.log (wrapper.debug ());
      expect(Amplify.Auth.confirmSignUp.mock.calls).toHaveLength(1);
      // call Auth.confirmSignUp with 2 argument which are username and confirmationCode
      expect(Amplify.Auth.confirmSignUp.mock.calls[0]).toHaveLength(2);
      expect(Amplify.Auth.signIn.mock.calls).toHaveLength(1);
      wrapper.unmount();
      done();
    });
  });
});
