import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import { Home } from '../home/Home';

let wrapper;

describe('Home Component', () => {
  it('snapshot matches correctly', () => {
    wrapper = mount(<Home isAuthenticated={false} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
