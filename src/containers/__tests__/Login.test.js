import React from 'react';
import { MemoryRouter } from 'react-router';
import { mount } from 'enzyme';
import { NavItem } from 'react-bootstrap';
import toJson from 'enzyme-to-json';
import { Provider } from 'react-redux';
import App from '../app/App';
import Home from '../home/Home';
import storeConfig from '../../store-config';

/* *****
 *  aws-amplify module is mocked in config/__mocks__/aws-amplify
 * see the detailed guideline at https://facebook.github.io/jest/docs/en/manual-mocks.html
 * ****** */
const Amplify = require('aws-amplify');

let _promise;
const store = storeConfig();

jest.mock('../../actions/request/index');
jest.mock('../../actions/request/comment/index', () => require('../../actions/request/comment/_mocks_/index')); // workaround for https://github.com/facebook/jest/issues/2070

describe('Login Component', () => {
  beforeEach(() => {
    /* **********************************************************************
     * overrite Auth.currentSession mock to simulate unauthenticated session
     ********************************************************************* */
    Amplify.Auth.currentSession = jest.fn().mockImplementation(() => {
      // promise = Promise.reject (new Error('No current user'));
      _promise = new Promise((resolve, reject) => process.nextTick(() => reject(new Error('No current user'))));
      return _promise;
    });
  });
  /*
  afterEach(() => {});
  */
  it('snapshot matches correctly', (done) => {
    const wrapper = mount(<Provider store={store}>
      <MemoryRouter initialEntries={[{ pathname: '/login', key: 'testKey' }]}>
        <App />
      </MemoryRouter>
    </Provider>);

    _promise.catch(() => {
      wrapper.update();
      expect(toJson(wrapper)).toMatchSnapshot();
      wrapper.unmount();
      done();
    });
  });

  it('call Auth.signIn correctly', (done) => {
    const wrapper = mount(<Provider store={store}>
      <MemoryRouter initialEntries={[{ pathname: '/login', key: 'testKey' }]}>
        <App />
      </MemoryRouter>
    </Provider>);

    _promise.catch(() => {
      wrapper.update();
      wrapper.find('form').simulate('submit');
      expect(Amplify.Auth.signIn.mock.calls).toHaveLength(1);
      // call Auth.signIn with 2 arguments which are username and password
      expect(Amplify.Auth.signIn.mock.calls[0]).toHaveLength(2);
      Amplify.Auth_signIn_promise.then(() => {
        /* *************************************************
         * After user has authenticated, redirect to /
         * and display Logout link at the top left corner.
        ************************************************** */
        wrapper.update();
        // console.log(wrapper.debug());
        // console.log(wrapper.find(NavItem).last().text())
        expect(wrapper.find(NavItem).last().text()).toEqual('Logout');
        expect(wrapper.containsMatchingElement(<Home />)).toBeTruthy();
        wrapper.unmount();
        done();
      });
    });
  });
});
