import React from 'react';
import { MemoryRouter } from 'react-router';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import toJson from 'enzyme-to-json';

import App from '../app/App';
import storeConfig from '../../store-config';
import { Home } from '../home/Home';
import NotFound from '../notfound/NotFound';
import Login from '../login/Login';
import Signup from '../signup/Signup';

// const ReduxAction = require('./actions')
const Amplify = require('aws-amplify');

let promise;
const store = storeConfig();

describe('App Component', () => {
  beforeEach(() => {
    /*
      ReduxAction.GetCurrentSession = jest.fn().mockImplementation (() => {
      promise = Promise.resolve ({session: {}});
      return promise;
    });
    original_currentSession = Amplify.Auth.currentSession
    Amplify.Auth.currentSession = jest.fn().mockImplementation (() => {
      promise = Promise.resolve (true);
      return promise;
    });
    */

    /* **********************************************************************
     * overrite Auth.currentSession mock to simulate unauthenticated session
     ********************************************************************** */
    Amplify.Auth.currentSession = jest.fn().mockImplementation(() => {
      // promise = Promise.reject (new Error('No current user'));
      promise = new Promise((resolve, reject) => process.nextTick(() => reject(new Error('No current user'))));
      return promise;
    });
  });

  afterEach(() => {
    // ReduxAction.GetCurrentSession.mockRestore()
    // Amplify.Auth.currentSession = original_currentSession
  });

  it('renders without crashing', () => {
    mount(<Provider store={store}>
      <MemoryRouter initialEntries={['/']}>
        <App />
      </MemoryRouter>
    </Provider>);
  });

  it('Snapshot match correctly', (done) => {
    const wrapper = mount(<Provider store={store}>
      <MemoryRouter initialEntries={[{ pathname: '/', key: 'testKey' }]}>
        <App />
      </MemoryRouter>
    </Provider>);

    promise.catch(() => {
      wrapper.update();
      expect(toJson(wrapper)).toMatchSnapshot();
      done();
    });
  });

  it('renders Home correctly', (done) => {
    const wrapper = mount(<Provider store={store}>
      <MemoryRouter initialEntries={[{ pathname: '/', key: 'testKey' }]}>
        <App />
      </MemoryRouter>
    </Provider>);

    promise.catch(() => {
      wrapper.update();
      const containHome = wrapper.containsMatchingElement(<Home />);
      expect(containHome).toBeTruthy();
      // console.log(Amplify.Auth.currentSession.mock.calls)
      done();
    });
  });

  it('renders Login correctly', (done) => {
    const wrapper = mount(<Provider store={store}>
      <MemoryRouter initialEntries={[{ pathname: '/login', key: 'testKey' }]}>
        <App />
      </MemoryRouter>
    </Provider>);
    promise.catch(() => {
      wrapper.update();
      const containLogin = wrapper.containsMatchingElement(<Login />);
      expect(containLogin).toBeTruthy();
      done();
    });
  });

  it('renders Signup correctly', (done) => {
    // jest.useFakeTimers()
    const wrapper = mount(<Provider store={store}>
      <MemoryRouter initialEntries={[{ pathname: '/signup', key: 'testKey' }]}>
        <App />
      </MemoryRouter>
    </Provider>);

    promise.catch(() => {
      // jest.runAllTimers()
      // console.log(wrapper.update ().debug());
      wrapper.update();
      const containSignup = wrapper.containsMatchingElement(<Signup />);
      expect(containSignup).toBeTruthy();
      done();
    });
  });

  it('renders NotFound correctly', (done) => {
    const wrapper = mount(<Provider store={store}>
      <MemoryRouter
        initialEntries={[{ pathname: '/notfound', key: 'testKey' }]}
      >
        <App />
      </MemoryRouter>
    </Provider>);

    promise.catch(() => {
      wrapper.update();
      const containNotFound = wrapper.containsMatchingElement(<NotFound />);
      expect(containNotFound).toBeTruthy();
      done();
    });
  });
});
