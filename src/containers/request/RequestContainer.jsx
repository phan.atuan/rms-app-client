import React, { ReactFragment } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import moment from 'moment';
import Request from '../../components/request/Index';
import { FetchRequestById, CloseRequestWithComment } from '../../actions/request';
import { AddRequestComment, FetchRequestComment } from '../../actions/request/comment';

class RequestContainer extends React.Component {
  constructor(props) {
    super(props);
    this.handerCloseRequestWithComment = this.handerCloseRequestWithComment.bind(this);
    this.handleAddComment = this.handleAddComment.bind(this);
    this.state = {
      isLoading: false,
      data: null,
    };
  }

  componentWillMount() {
    this.setState({
      isLoading: true,
      data: null,
    });
    const promise1 = FetchRequestById(this.props.requestId);
    const promise2 = FetchRequestComment(this.props.requestId, 1);
    Promise.all([promise1, promise2])
      .then(responses =>
        this.setState({
          isLoading: false,
          data: Object.assign({}, { ...responses[0].item }, { comment: responses[1].items.length > 0 ? responses[1].items[0].text : '' }),
        }))
      .catch(error => this.props.createNotification('error', error.message)());
  }

  handerCloseRequestWithComment(requestId, comment, status, submissionDate) {
    const action = status === 'Close' ? 'closed' : 'cancelled';
    const fulfilmentDate = moment();
    return CloseRequestWithComment(requestId, comment, status, submissionDate)
      .then(() => this.props.createNotification('success', `Request was ${action} successful`)())
      .then(() => {
        if (this.state.data.status === status) {
          this.setState(prevState =>
            ({ data: Object.assign({}, prevState.data, { status, comment, fulfilmentDate }) }));
        }
        return true;
      })
      .then(() => this.props.shouldRefreshRequestList(status))
      .catch((error) => {
        this.props.createNotification('error', error.message)();
      });
  }

  handleAddComment(requestId, comment) {
    return AddRequestComment(requestId, comment)
      .then((response) => {
        this.props.createNotification('success', 'Comment was added successful')();
        return response;
      })
      .then(() => this.setState(prevState =>
        ({ data: Object.assign({}, prevState.data, { comment }) })))
      .catch(error => this.props.createNotification('error', error.message)());
  }

  render() {
    if (this.state.data) {
      return (
        <Request
          // fetchTheMostRecentComment={FetchRequestComment}
          data={this.state.data}
          addRequestComment={this.handleAddComment}
          closeRequestWithComment={this.handerCloseRequestWithComment}
          {...this.props}
        />);
    }

    return (
      <div>
        Loading...
      </div>
    );
  }
}
export default withRouter(RequestContainer);

RequestContainer.propTypes = {
  requestId: PropTypes.string.isRequired,
  showComment: PropTypes.func.isRequired,
  createNotification: PropTypes.func.isRequired,
  shouldRefreshRequestList: PropTypes.func.isRequired,
};
