import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import List from '../../components/request/List';
import { FetchRequestList } from '../../actions/request';
import { AddRequestComment, FetchRequestComment } from '../../actions/request/comment';
// handleRequestDelete

const mapStateToProps = state =>
  ({
    isFetching: state.request.list.isFetching,
    isSubmitting: state.request.list.isSubmitting,
    items: state.request.list.items,
    errors: state.request.list.errors,
    page: state.request.list.page,
  });

const mapDispatchToProps = dispatch =>
  ({
    fetchRequestList: status => dispatch(FetchRequestList(status)),
    addRequestComment: (requestId, text) => dispatch(AddRequestComment(requestId, text)),
    fetchCommentList: requestId => dispatch(FetchRequestComment(requestId)),
  });

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(List));
