import { connect } from 'react-redux';
import RequestForm from '../../components/request/Form';
import { FetchRequestById, SaveRequest, ChangeRequestProp, ResetFormData, DeleteRequest } from '../../actions/request';
import { AddRequestComment, FetchRequestComment } from '../../actions/request/comment';
// handleRequestDelete


const mapStateToProps = (state, ownProps) =>
  ({
    id: ownProps.match.params.id,
    isFetching: state.request.form.isFetching,
    isSubmitting: state.request.form.isSubmitting,
    data: state.request.form.data,
    // success: state.request.form.success,
    errors: state.request.form.errors,
    // hasError: state.request.form.hasError,
    // help: state.request.form.help,
  });

const mapDispatchToProps = dispatch =>
  ({
    fetchRequestById: id => dispatch(FetchRequestById(id)),
    addRequestComment: (requestId, text) => dispatch(AddRequestComment(requestId, text)),
    fetchCommentList: requestId => dispatch(FetchRequestComment(requestId)),
    reset: () => dispatch(ResetFormData()),
    handleChange: event => dispatch(ChangeRequestProp(event.target)),
    handleSubmit: (isDelete = false, that) => {
      let promise;
      if (isDelete) promise = dispatch(DeleteRequest());
      else promise = dispatch(SaveRequest());
      promise.then(() => {
        if (!that.props.errors) {
          that.props.history.push('/');
        }
      });
    },
  });

export default connect(mapStateToProps, mapDispatchToProps)(RequestForm);
