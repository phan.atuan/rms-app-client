import React from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import RequestForm from '../../components/request/Form';
import moment from 'moment';
import { FetchRequestById, SaveRequest, DeleteRequest } from '../../actions/request';
import { FetchRequestComment } from '../../actions/request/comment';

const SYSTEM_ERROR_MESSAGE = 'Unexpected error happened, please contact system administrator for assistant';

function reset() {
  return (
    {
      isLoading: false,
      isSubmitting: false,
      request: { status: 'Open' },
      errors: {},
      comment: {
        isLoading: false,
        items: [],
        errors: {},
      },
    }
  );
}
class FormContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = reset();
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillMount() {
    const id = this.props.match.params.id;
    if (id !== 'create') {
      this.setState({
        isLoading: true,
        isSubmitting: false,
        request: {},
        errors: {},
        comment: {
          isLoading: true,
          items: [],
          errors: {},
        }
      });
      const promise1 = FetchRequestById(id);
      const promise2 = FetchRequestComment(id);
      Promise.all([promise1, promise2])
        .then((response) => {
          let requestState;
          let commentState;
          if (response[0].status) {
            const request = response[0].item;
            const { tentativeStartDate, submissionDate, fulfilmentDate } = request;
            request.tentativeStartDate = tentativeStartDate ? moment(tentativeStartDate).format('YYYY-MM-DD') : null;
            request.submissionDate = submissionDate ? moment(submissionDate).format('YYYY-MM-DD') : null;
            request.fulfilmentDate = fulfilmentDate ? moment(fulfilmentDate).format('YYYY-MM-DD') : null;
            requestState = {
              isLoading: false,
              isSubmitting: false,
              request,
              errors: {},
            };
          } else {
            requestState = {
              isLoading: false,
              isSubmitting: false,
              request: {},
              errors: response[0].errors,
            };
          }

          if (response[1].status) {
            commentState = {
              isLoading: false,
              items: response[1].items,
              errors: {},
            };
          } else {
            commentState = {
              isLoading: false,
              items: [],
              errors: response[1].errors,
            };
          }

          this.setState({ ...requestState, comment: { ...commentState } });
        })
        .catch(() =>
          this.setState(Object.assign(
            {},
            reset(),
            {
              errors: {
                system: SYSTEM_ERROR_MESSAGE,
              },
            },
          )));
    } else {
      this.setState(reset());
    }
  }

  handleChange(event) {
    const target = event.target;
    this.setState(prevState =>
      ({
        request: Object.assign({}, prevState.request, { [target.id]: target.value }),
      }));
  }

  handleSubmit(isDelete = false) {
    let promise;
    const { request } = this.state;
    const { tentativeStartDate, submissionDate, fulfilmentDate } = request;
    request.tentativeStartDate = tentativeStartDate && tentativeStartDate.length > 0 ? tentativeStartDate : null;
    request.submissionDate = submissionDate && submissionDate.length > 0 ? submissionDate : null;
    request.fulfilmentDate = fulfilmentDate && fulfilmentDate.length > 0 ? fulfilmentDate : null;
    this.setState({
      isSubmitting: true,
    });
    if (isDelete) promise = DeleteRequest(request.requestId);
    else promise = SaveRequest(request);
    promise.then((response) => {
      const { status } = response;
      if (status) {
        this.props.history.push('/');
      } else {
        this.setState({
          isSubmitting: false,
          errors: response.errors,
        });
      }
    });
    promise.catch(() => {
      this.setState({
        isSubmitting: false,
        errors: {
          system: SYSTEM_ERROR_MESSAGE
        }
      });
    });
  }

  render() {
    return (
      <RequestForm
        id={this.props.match.params.id}
        data={this.state}
        handleChange={this.handleChange}
        handleSubmit={this.handleSubmit}
        {...this.props}
      />
    );
  }
}

export default withRouter(FormContainer);

FormContainer.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

