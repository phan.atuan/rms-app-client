import React from 'react';
// import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import List from '../../components/request/List';
import { FetchRequestList } from '../../actions/request';

const PAGE_SIZE = 3;

function reset(status) {
  return ({
    items: [],
    isFetching: false,
    status,
    page: 0,
    tokens: [null],
  });
}
class ListContainer extends React.Component {
  constructor(props) {
    super(props);
    this.goToPage = this.goToPage.bind(this);
    this.filterByStatus = this.filterByStatus.bind(this);
    this.loadRequestList = this.loadRequestList.bind(this);
    this.shouldRefreshRequestList = this.shouldRefreshRequestList.bind(this);
    this.state = reset('Open');
  }

  componentWillMount() {
    const { status } = this.state;
    this.loadRequestList(status, 0, 'next', PAGE_SIZE);
  }

  loadRequestList(status, page, direction, pageSize) {
    this.setState({ isFetching: true, items: [] });
    const { tokens } = this.state;
    // let { page } = this.state;
    // this.state represented the RENDERED state, it is not reliable to get this.state right after this.setState()
    let token = null;
    switch (direction) {
      case 'next':
        if (page > tokens.length) {
          token = tokens[tokens.length - 1];
        } else {
          token = tokens[page];
        }
        break;
      default: // previous
        if (page < 2) {
          // Should disable Prev button
        } else {
          token = tokens[page - 2];
        }
    }
    page = direction === 'next' ? page + 1 : page - 1;
    FetchRequestList(status, token, pageSize)
      .then(response =>
        this.setState(() => {
          if (page < tokens.length) {
            tokens[page] = response.token;
          } else {
            tokens.push(response.token);
          }
          return ({
            items: response.items,
            isFetching: false,
            tokens,
            page,
          });
        }));
  }

  goToPage(direction) {
    const { status, page } = this.state;
    this.loadRequestList(status, page, direction, PAGE_SIZE);
  }

  filterByStatus(status) {
    this.setState(Object.assign({}, reset(status)));
    this.loadRequestList(status, 0, 'next', PAGE_SIZE);
  }

  shouldRefreshRequestList(newStatus) {
    const {
      status,
    } = this.state;
    if (status !== newStatus) { // reload the current page
      const { page } = this.state;
      this.loadRequestList(status, page - 1, 'next', PAGE_SIZE);
      return true;
    }
    return false;
  }

  render() {
    const {
      items,
      isFetching,
      page,
      tokens,
    } = this.state;
    const previousDisabled = page <= 1;
    const nextDisabled = !tokens[page];
    return (
      <List
        {...{
          items,
          isFetching,
          nextDisabled,
          previousDisabled,
        }}
        goToPage={this.goToPage}
        filterByStatus={this.filterByStatus}
        shouldRefreshRequestList={this.shouldRefreshRequestList}
      />);
  }
}

export default withRouter(ListContainer);

ListContainer.propTypes = {

};

ListContainer.defaultProps = {
};
