import React from 'react';
import { Button, Modal } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { AddRequestComment, FetchRequestComment } from '../../../actions/request/comment';
import CommentList from './List';

export default class CommentModal extends React.Component {
  constructor(props) {
    super(props);
    this.addComment = this.addComment.bind(this);
    this.fetchCommentList = this.fetchCommentList.bind(this);
  }

  addComment(text) {
    AddRequestComment(this.props.requestId, text)
      .then((response) => {
        this.props.createNotification('success', 'Comment was added successful')();
        return response;
      })
      .then(() => this.props.hideComment())
      .catch((error) => {
        this.props.createNotification('error', error.message)();
      });
  }

  fetchCommentList() {
    return FetchRequestComment(this.props.requestId);
  }

  render() {
    return (
      <Modal
        show={this.props.show}
        onHide={this.props.hideComment}
        aria-labelledby="ModalHeader"
      >
        <Modal.Header closeButton>
          <Modal.Title id="ModalHeader">Comments</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <CommentList fetchCommentList={this.fetchCommentList} addComment={this.addComment} />
        </Modal.Body>
        <Modal.Footer>
          <Button bsStyle="primary" onClick={this.props.hideComment}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

CommentModal.propTypes = {
  show: PropTypes.bool.isRequired,
  hideComment: PropTypes.func.isRequired,
  createNotification: PropTypes.func.isRequired,
  requestId: PropTypes.string.isRequired,
};
