
import React, { Fragment } from 'react';
import {
  Button, FormControl,
  FormGroup, InputGroup, Form,
  ListGroup, ListGroupItem } from 'react-bootstrap';
import PropTypes from 'prop-types';
import moment from 'moment';

export default class CommentList extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleAddComment = this.handleAddComment.bind(this);
    this.state = {
      isLoading: false,
      items: [],
      text: '',
    };
  }

  componentWillMount() {
    this.setState({ isLoading: true });
    this.props.fetchCommentList()
      .then(response => this.setState({
        isLoading: false,
        items: response.items,
      }));
  }

  handleChange(event) {
    this.setState({ text: event.target.value });
  }

  handleAddComment(event) {
    event.preventDefault();
    this.props.addComment(this.state.text);
  }

  render() {
    if (this.state.isLoading) {
      return (<Fragment>Loading comments ...</Fragment>);
    }
    const children = this.state.items.map(c =>
      (
        <ListGroupItem
          key={c.commentId}
          header={c.text}
        >
          {moment(c.createdAt, 'YYYY-MM-DD, HH:mm:ss.SSS').format('MMMM Do YYYY')}
        </ListGroupItem>));
    if (children.length > 0) {
      return (
        <ListGroup>
          {children}
          <Form onSubmit={this.handleAddComment}>
            <FormGroup>
              <InputGroup>
                <FormControl type="text" value={this.state.text} onChange={this.handleChange} />
                <InputGroup.Button>
                  <Button type="submit">Add</Button>
                </InputGroup.Button>
              </InputGroup>
            </FormGroup>
          </Form>
        </ListGroup>);
    }
    return (<Fragment>There is no comments</Fragment>);
  }
}

CommentList.propTypes = {
  fetchCommentList: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
};

CommentList.defaultProps = {
};
