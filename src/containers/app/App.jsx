import React, { Component, Fragment } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { GetCurrentSession, SignOut } from '../../actions';
import Routes from '../../Routes';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAuthenticating: true,
    };
    this.handleLogout = this.handleLogout.bind(this);
  }

  componentDidMount() {
    this.props
      .getCurrentSession()
      .catch((error) => {
        if (error !== 'No current user') {
          alert(`App.js: ${error}`);
        }
      });
    this.setState({ isAuthenticating: false });
  }

  handleLogout() {
    this.props
      .signOut()
      .then(() => {
        // this.props.userHasAuthenticated (false);
        this.props.history.push('/login');
      })
      .catch(error => console.log(error));
  }

  render() {
    return (
      !this.state.isAuthenticating &&
      <div className="App container">
        <Navbar fluid collapseOnSelect>
          <Navbar.Header>
            <Navbar.Brand>
              <Link to="/" href="/">RMS</Link>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav pullRight>
              {
                this.props.isAuthenticated
                ?
                  <Fragment>
                    <NavItem componentClass={Link} href="/request/create" to="/request/create">Create</NavItem>
                    <NavItem onClick={this.handleLogout}>Logout</NavItem>
                  </Fragment>
                :
                  <Fragment>
                    <NavItem componentClass={Link} href="/signup" to="/signup">Signup</NavItem>
                    <NavItem componentClass={Link} href="/login" to="/login">Login</NavItem>
                  </Fragment>
              }
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Routes />
      </div>
    );
  }
}

App.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  getCurrentSession: PropTypes.func.isRequired,
  signOut: PropTypes.func.isRequired,
  history: PropTypes.objectOf(PropTypes.any).isRequired,
};

const mapStateToProps = state => Object.assign({}, { isAuthenticated: state.app.isAuthenticated });

const mapDispatchToProps = dispatch =>
  Object.assign({}, {
    getCurrentSession: () => dispatch(GetCurrentSession()),
    signOut: () => dispatch(SignOut()),
  });


const AppContainer = connect(mapStateToProps, mapDispatchToProps)(App);
export default withRouter(AppContainer);
