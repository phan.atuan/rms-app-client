import moment from 'moment';

const ahlRequest = {
  _id: 'request_nnn',
  accountName: 'Aussie Home Loan',
  resourceType: 'PHP developer',
  resourceRate: 162,
  quantity: 2,
  submissionDate: moment('15/03/2018', 'DD/MM/YYYY').valueOf(),
  tentativeStartDate: moment('15/03/2018', 'DD/MM/YYYY').valueOf(),
  fulfilmentDate: null,
  status: 'Open',
  comments: [],
};

const sgRequest = {
  _id: 'request_mmm',
  accountName: 'Study Group',
  resourceType: 'Cognos developer',
  resourceRate: 240,
  quantity: 1,
  submissionDate: moment('21/05/2018', 'DD/MM/YYYY').valueOf(),
  tentativeStartDate: moment('21/05/2018', 'DD/MM/YYYY').valueOf(),
  fulfilmentDate: null,
  status: 'Open',
  comments: [],
};

const baseUrl = 'http://localhost:3000';

export default {
  ahlRequest,
  sgRequest,
  baseUrl,
};

