import React from 'react';
import { Route } from 'react-router';
import { action } from '@storybook/addon-actions';
import Request from '../../components/request/Index';
import store from './store';

export default () =>
  (<Route render={props =>
    (<Request
      data={store.ahlRequest}
      baseUrl={store.baseUrl}
      handleAddComment={action('handleAddComment')}
      handleCloseRequestWithComment={action('handleCloseRequestWithComment')}
      handleShowComment={action('handleShowComment')}
      {...props}
    />)
    }
  />);
