import React from 'react';
import { Route } from 'react-router';
import { action } from '@storybook/addon-actions';
import Form from '../../components/request/Form';
import store from './store';

export default () =>
  (<Route render={props =>
    (<Form
      isFetching={false}
      isSubmitting={false}
      showComment={false}
      data={store.ahlRequest}
      success={false}
      help={{
        accountName: '',
        resourceType: '',
        resourceRate: '',
        quantity: '',
        tentativeStartDate: '',
        fulfilmentDate: '',
      }}
      handleChange={action('handleChange')}
      fetchResourceRequest={action('fetchResourceRequest')}
      handleSubmit={action('handleSubmit')}
      {...props}
    />)
    }
  />);
