import React from 'react';
import { Route } from 'react-router';
import { action } from '@storybook/addon-actions';
import List from '../../components/request/List';
import store from './store';

export default () =>
  (<Route render={props =>
    (<List
      items={[store.ahlRequest, store.sgRequest]}
      baseUrl={store.baseUrl}
      isFetching={false}
      showComment={false}
      handleHideComment={action('handleHideComment')}
      handleAddComment={action('handleAddComment')}
      handleShowComment={action('handleShowComment')}
      fetchResourceRequests={action('fetchResourceRequests')}
      handleCloseRequestWithComment={action('handleCloseRequestWithComment')}
      {...props}
    />)
    }
  />);
