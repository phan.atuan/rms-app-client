import React from 'react';
import { storiesOf } from '@storybook/react';
import StoryRouter from 'storybook-react-router';
import IndexWrapper from './request/Index';
import ListWrapper from './request/List';
import FormWrapper from './request/Form';


storiesOf('Request', module)
  .addDecorator(StoryRouter())
  .addWithJSX('Index', () => <IndexWrapper />);

storiesOf('Request', module)
  .addDecorator(StoryRouter())
  .addWithJSX('List', () => <ListWrapper />);

storiesOf('Request', module)
  .addDecorator(StoryRouter())
  .addWithJSX('Form', () => (<FormWrapper />));
